### uruchomienie serwera
`` symfony server:start``
(z poziomu folderu ./api)
### konfiguracja serwera mailowego
``./api/.env`` - ustawić dane dostępowe do serwera (np. mailtrapa) w kluczu MAILER_DSN
### lokalizacja plików bazy danych
- ``./api/assets/db.json`` - baza danych odbiorców wiadomości
- ``./api/assets/sms.csv`` - plik w którym zapisywana jest treść wysyłanych smsów

inną lokalizację można ustawić w pliku `api/src/Infrastructure/config/services.yaml`
### wystawione endpointy
- ``[GET] /user`` - zwraca wszystkich użytkowników
- ``[GET] /user/search/{login}`` - zwraca użytkownika o podanym loginie
- ``[POST] /notify``, ``params: { login: array, message: string}`` - wysyła powiadomienie do wskazanych użytkowników
