<?php
/** @author: Roman Kowalski */

namespace App\Infrastructure\Repository;

use App\Application\DTO\SmsDTO;
use App\Domain\Repository\SmsRepository;

class SmsFileRepository implements SmsRepository
{
    private $smsFilePath;

    public function __construct(string $smsFilePath)
    {
        $this->smsFilePath = $smsFilePath;
    }

    /**
     * @param SmsDTO $data
     */
    public function send(SmsDTO $data): void
    {
        $file = fopen($this->smsFilePath, 'a');
        fputcsv($file, [
            $data->getCreatedAt()->format('Y-m-d H:i:s'),
            $data->getPhoneNumber(),
            $data->getContent()
        ]);
    }
}