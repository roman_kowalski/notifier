<?php
/** @author: Roman Kowalski */

namespace App\Infrastructure\Repository;

use App\Domain\Entity\User;
use App\Domain\Exception\NotSupportedChannelTypeException;
use App\Domain\Repository\UserRepository;
use Closure;

class JsonUserRepository implements UserRepository
{
    private $dbFilePath;

    /**
     * JsonUserRepository constructor.
     * @param string $jsonDbPath
     */
    public function __construct(string $jsonDbPath)
    {
        $this->dbFilePath = $jsonDbPath;
    }

    /**
     * @return User[]
     * @throws NotSupportedChannelTypeException
     */
    public function findAll(): array
    {
        return $this->decodeAndTest(function() { return true; });
    }

    /**
     * @param string[] $login
     * @return User[]
     * @throws NotSupportedChannelTypeException
     */
    public function findByLogin(array $login): array
    {
        $haystack = array_map(function($item) { return strtolower($item); }, $login);
        $foundItems = $this->decodeAndTest(
            function ($user) use ($haystack) { return in_array(strtolower($user['login']), $haystack); }
        );
        return $foundItems;
    }

    /**
     * @param Closure $callback
     * @return User[]
     * @throws NotSupportedChannelTypeException
     */
    protected function decodeAndTest(Closure $callback): array
    {
        return array_map(
            function ($data) {
                return new User(
                    $data['login'],
                    $data['email'],
                    $data['phone'],
                    $data['channels']);
            },
            array_values(array_filter($this->getRawData(), $callback))
        );
    }

    protected function getRawData(): array
    {
        return json_decode(file_get_contents($this->dbFilePath), true);
    }
}