<?php

namespace App\Infrastructure\Controller;

use App\Domain\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Infrastructure\Controller
 */
class UserController extends AbstractController
{
    /**
     * @Route("/user", methods={"GET"})
     * @param Request $request
     * @param UserRepository $repository
     * @return JsonResponse
     */
    public function fetchUsers(Request $request, UserRepository $repository)
    {
        $items = $repository->findAll();
        return $this->json($items);
    }

    /**
     * @Route("/user/search/{login}", methods={"GET"})
     * @param string $login
     * @param UserRepository $repository
     * @return JsonResponse
     */
    public function findByLogin(string $login, UserRepository $repository)
    {
        $user = $repository->findByLogin([$login]);
        return $this->json($user);
    }
}