<?php
/** @author: Roman Kowalski */

namespace App\Infrastructure\Controller;

use App\Application\Command\CreateNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class NotifyController extends AbstractController
{
    /**
     * @Route("/notify", methods={"POST"})
     * @param Request $request
     * @param MessageBusInterface $messageBus
     * @return JsonResponse
     */
    public function sendNotification(Request $request, MessageBusInterface $messageBus)
    {
        $parameters = json_decode($request->getContent(), true);

        if (!$parameters['login'] || empty($parameters['login'])) {
            return $this->json(['error' => 'login is not set'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        if (!$parameters['message']) {
            return $this->json(['error' => 'message is not set'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $messageBus->dispatch(new CreateNotification(
            is_array($parameters['login']) ? $parameters['login'] : [$parameters['login']],
            $parameters['message'])
        );

        return $this->json(['Notifications were successfully sent!']);
    }
}