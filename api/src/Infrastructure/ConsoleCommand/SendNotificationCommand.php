<?php
/** @author: Roman Kowalski */

namespace App\Infrastructure\ConsoleCommand;

use App\Application\Command\CreateNotification;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class SendNotificationCommand extends Command
{
    private $messageBus;
    protected static $defaultName = 'send-notification';

    /**
     * SendNotificationCommand constructor.
     * @param MessageBusInterface $messageBus
     */
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct();
        $this->messageBus = $messageBus;
    }

    protected function configure()
    {
        parent::configure();
        $this
            ->setDescription('Sends text message to user')
            ->setHelp('Sends text message to specified users using assigned communication channels. Currenty email and sms are supported.')
            ->addArgument('m', InputArgument::REQUIRED)
            ->addArgument('u', InputArgument::REQUIRED | InputArgument::IS_ARRAY)
            ->addOption('message', 'm')
            ->addOption('user', 'u')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->messageBus->dispatch(
            new CreateNotification($input->getArgument('u'), $input->getArgument('m'))
        );
        $output->writeln('Notifications were successfully sent!');
        return 0;
    }
}