<?php
/** @author: Roman Kowalski */

namespace App\Domain\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * Class ChannelType
 *
 * @package App\Domain\ValueObject
 * @method static ChannelType EMAIL()
 * @method static ChannelType SMS()
 */
class ChannelType extends Enum
{
    private const EMAIL = 'email';
    private const SMS = 'sms';
}