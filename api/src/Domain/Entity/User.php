<?php
/** @author: Roman Kowalski */

namespace App\Domain\Entity;

use App\Domain\Exception\NotSupportedChannelTypeException;
use App\Domain\ValueObject\ChannelType;

class User
{
    private $login;
    private $email;
    private $phone;
    private $channels = [];

    /**
     * User constructor.
     * @param string $login
     * @param string $email
     * @param string $phone
     * @param array $channels
     * @throws NotSupportedChannelTypeException
     */
    public function __construct(string $login, string $email, string $phone, array $channels = [])
    {
        $this->login = $login;
        $this->email = $email;
        $this->phone = $phone;
        foreach ($channels as $channel) {
            if (false === ChannelType::isValid($channel)) {
                throw new NotSupportedChannelTypeException(
                    sprintf("Not supported channel type: %s", $channel)
                );
            }
            $this->channels[] = new ChannelType($channel);
        }
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return array
     */
    public function getChannels(): array
    {
        return $this->channels;
    }
}