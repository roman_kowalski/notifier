<?php
/** @author: Roman Kowalski */

namespace App\Domain\Repository;

use App\Application\DTO\SmsDTO;

interface SmsRepository
{
    public function send(SmsDTO $data): void;
}