<?php

namespace App\Domain\Repository;

use App\Domain\Entity\User;

interface UserRepository
{
    /**
     * @return User[]
     */
    public function findAll(): array;

    /**
     * @param string[] $login
     * @return User[]
     */
    public function findByLogin(array $login): array;
}