<?php
/** @author: Roman Kowalski */

namespace App\Application\DTO;


class SmsDTO
{
    private $phoneNumber;
    private $createdAt;
    private $content;

    public function __construct(string $phoneNumber, \DateTime $createdAt, string $content)
    {
        $this->phoneNumber = $phoneNumber;
        $this->createdAt = $createdAt;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}