<?php
/** @author: Roman Kowalski */

namespace App\Application\DTO;


use App\Domain\Exception\NotSupportedChannelTypeException;
use App\Domain\ValueObject\ChannelType;

class UserDTO
{
    private $login;
    private $email;
    private $phone;
    private $channels = [];

    /**
     * UserDTO constructor.
     * @param $login
     * @param $email
     * @param $phone
     * @param ChannelType[] $channels
     * @throws NotSupportedChannelTypeException
     */
    public function __construct($login, $email, $phone, array $channels)
    {
        $this->login = $login;
        $this->email = $email;
        $this->phone = $phone;
        foreach ($channels as $channel) {
            if (false === $channel instanceof ChannelType) {
                throw new NotSupportedChannelTypeException();
            }
            $this->channels[] = $channel;
        }
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return ChannelType[]
     */
    public function getChannels(): array
    {
        return $this->channels;
    }
}