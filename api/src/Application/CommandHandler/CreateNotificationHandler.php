<?php
/** @author: Roman Kowalski */

namespace App\Application\CommandHandler;

use App\Application\Command\CreateNotification;
use App\Application\DTO\UserDTO;
use App\Domain\Exception\UserNotFoundException;
use App\Domain\Repository\UserRepository;
use App\Domain\ValueObject\ChannelType;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateNotificationHandler implements MessageHandlerInterface
{
    private $userRepository;
    private $messageBus;

    /**
     * CreateNotificationHandler constructor.
     * @param UserRepository $userRepository
     * @param MessageBusInterface $messageBus
     */
    public function __construct(UserRepository $userRepository, MessageBusInterface $messageBus)
    {
        $this->userRepository = $userRepository;
        $this->messageBus = $messageBus;
    }

    public function __invoke(CreateNotification $command)
    {
        // get user object
        $users = $this->userRepository->findByLogin($command->getRecipients());
        if (empty($users)) {
            throw new UserNotFoundException(
                sprintf("Couldn't find any user by given login: %s", implode(', ', $command->getRecipients()))
            );
        }
        $userDTOs = array_map(function($user) {
            return new UserDTO(
                $user->getLogin(),
                $user->getEmail(),
                $user->getPhone(),
                $user->getChannels()
            );
        }, $users);

        foreach ($userDTOs as $user) {
            $channels = $user->getChannels();
            if (empty($channels)) {
                $channels[] = ChannelType::EMAIL();
            }
            foreach ($channels as $channel) {
                /** @var $channel ChannelType */
                $commandName = sprintf("\App\Application\Command\Send%s", ucfirst($channel->getValue()));
                if (class_exists($commandName)) {
                    $this->messageBus->dispatch(new $commandName($user, $command->getMessage()));
                }
            }
        }
    }
}