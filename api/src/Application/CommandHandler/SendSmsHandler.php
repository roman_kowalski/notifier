<?php
/** @author: Roman Kowalski */

namespace App\Application\CommandHandler;

use App\Application\Command\SendSms;
use App\Application\DTO\SmsDTO;
use App\Domain\Repository\SmsRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SendSmsHandler implements MessageHandlerInterface
{
    private $smsRepository;

    public function __construct(SmsRepository $smsRepository)
    {
        $this->smsRepository = $smsRepository;
    }

    public function __invoke(SendSms $command)
    {
        $this->smsRepository->send(new SmsDTO(
                $command->getUserDTO()->getPhone(),
                new \DateTime(),
                $command->getMessage()
            )
        );
    }
}