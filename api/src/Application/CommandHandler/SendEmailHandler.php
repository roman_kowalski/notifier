<?php
/** @author: Roman Kowalski */

namespace App\Application\CommandHandler;

use App\Application\Command\SendEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Email;

class SendEmailHandler implements MessageHandlerInterface
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(SendEmail $command)
    {
        $email = (new Email())
            ->from('notifier@dealer.dev')
            ->to($command->getUserDTO()->getEmail())
            ->subject('notification')
            ->text($command->getMessage())
            ->html(sprintf("<p>%s</p>", $command->getMessage()));

        $this->mailer->send($email);
    }
}