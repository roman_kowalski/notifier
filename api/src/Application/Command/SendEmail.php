<?php
/** @author: Roman Kowalski */

namespace App\Application\Command;

use App\Application\DTO\UserDTO;

class SendEmail
{
    private $userDTO;
    private $message;

    /**
     * SendEmail constructor.
     * @param UserDTO $userDTO
     * @param string $message
     */
    public function __construct(UserDTO $userDTO, string $message)
    {
        $this->userDTO = $userDTO;
        $this->message = $message;
    }

    /**
     * @return UserDTO
     */
    public function getUserDTO(): UserDTO
    {
        return $this->userDTO;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}