<?php
/** @author: Roman Kowalski */

namespace App\Application\Command;

use App\Application\DTO\UserDTO;

class SendSms
{
    private $userDTO;
    private $message;

    public function __construct(UserDTO $userDTO, string $message)
    {
        $this->userDTO = $userDTO;
        $this->message = $message;
    }

    /**
     * @return UserDTO
     */
    public function getUserDTO(): UserDTO
    {
        return $this->userDTO;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}