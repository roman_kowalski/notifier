<?php
/** @author: Roman Kowalski */

namespace App\Application\Command;

class CreateNotification
{
    private $recipients;
    private $message;

    public function __construct(array $recipients, string $message)
    {
        $this->recipients = $recipients;
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}