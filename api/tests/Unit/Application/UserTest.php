<?php
/** @author: Roman Kowalski */

namespace App\Tests\Unit\Application;

use App\Domain\Entity\User;
use App\Domain\Exception\NotSupportedChannelTypeException;
use App\Domain\ValueObject\ChannelType;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testShouldCreateUser()
    {
        // Given & When
        $user = new User('jane', 'jane@test.pl', '+48123456123', ['email']);
        // Then
        $this->assertSame('jane', $user->getLogin());
        $this->assertSame('jane@test.pl', $user->getEmail());
        $this->assertSame('+48123456123', $user->getPhone());
        $this->assertEquals([ChannelType::EMAIL()], $user->getChannels());
    }
    public function testShouldThrowNotSupportedChannelTypeExceptionWhenNotSupportedChannelIsUsed()
    {
        // Expect
        $this->expectException(NotSupportedChannelTypeException::class);
        // Given & When
        new User('jane', 'jane@test.pl', '+48123456123', ['email', 'sms', 'pigeon']);
    }
}