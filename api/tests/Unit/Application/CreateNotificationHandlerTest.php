<?php
/** @author: Roman Kowalski */

namespace App\Tests\Unit\Application;

use App\Application\Command\CreateNotification;
use App\Application\Command\SendEmail;
use App\Application\Command\SendSms;
use App\Application\CommandHandler\CreateNotificationHandler;
use App\Domain\Entity\User;
use App\Domain\ValueObject\ChannelType;
use App\Infrastructure\Repository\JsonUserRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBus;

class CreateNotificationHandlerTest extends TestCase
{
    private $userRepository;
    /** @var \PHPUnit\Framework\MockObject\MockObject|MessageBus */
    private $bus;

    protected function setUp(): void
    {
        $this->userRepository = $this->createMock(JsonUserRepository::class);
        $this->bus = $this->createMock(MessageBus::class);
    }

    public function testShouldDispatchCreateEmailCommandWhenChannelTypeHasEmail()
    {
        // Given
        $user = new User('peter', 'test@test.pl', '505070800', ['email']);
        $this->userRepository->method('findByLogin')->willReturn([$user]);
        $handlerUnderTest = new CreateNotificationHandler($this->userRepository, $this->bus);
        $command = new CreateNotification([$user->getLogin()], 'Hello world');

        // When & Then
        $this->bus
            ->expects($this->once())
            ->method('dispatch')
            ->with(
                $this->callback(
                    function (SendEmail $command) use ($user) {
                        return $command->getUserDTO()->getEmail() === $user->getEmail();
                    }
                )
            )
            ->willReturnCallback(function ($command) {
                return new Envelope($command);
            });

        $handlerUnderTest($command);
    }

    public function testShouldDispatchCreateSmsCommandWhenChannelTypeHasSms()
    {
        // Given
        $user = new User('peter', 'test@test.pl', '505070800', ['sms']);
        $this->userRepository->method('findByLogin')->willReturn([$user]);
        $handlerUnderTest = new CreateNotificationHandler($this->userRepository, $this->bus);
        $command = new CreateNotification([$user->getLogin()], 'Hello world');

        // When & Then
        $this->bus
            ->expects($this->once())
            ->method('dispatch')
            ->with(
                $this->callback(
                    function (SendSms $command) use ($user) {
                        return $command->getUserDTO()->getPhone() === $user->getPhone();
                    }
                )
            )
            ->willReturnCallback(function ($command) {
                return new Envelope($command);
            });

        $handlerUnderTest($command);
    }

    public function testShouldDispatchTwoCommandsWhenChannelTypeHasEmailAndSms()
    {
        // Given
        $user = new User('peter', 'test@test.pl', '505070800', [
            ChannelType::EMAIL()->getValue(),
            ChannelType::SMS()->getValue()
        ]);
        $this->userRepository->method('findByLogin')->willReturn([$user]);
        $handlerUnderTest = new CreateNotificationHandler($this->userRepository, $this->bus);
        $command = new CreateNotification([$user->getLogin()], 'Hello world');

        // When & Then
        $this->bus
            ->expects($this->exactly(2))
            ->method('dispatch')
            ->willReturnCallback(function ($command) {
                return new Envelope($command);
            });

        $handlerUnderTest($command);
    }

    public function testShouldDispatchCreateEmailCommandWhenNoChannelIsSpecified()
    {
        // Given
        $user = new User('peter', 'test@test.pl', '505070800', []);
        $this->userRepository->method('findByLogin')->willReturn([$user]);
        $handlerUnderTest = new CreateNotificationHandler($this->userRepository, $this->bus);
        $command = new CreateNotification([$user->getLogin()], 'Hello world');

        // When & Then
        $this->bus
            ->expects($this->once())
            ->method('dispatch')
            ->with(
                $this->callback(
                    function (SendEmail $command) use ($user) {
                        return $command->getUserDTO()->getEmail() === $user->getEmail();
                    }
                )
            )
            ->willReturnCallback(function ($command) {
                return new Envelope($command);
            });

        $handlerUnderTest($command);
    }

    public function testShouldDispatchTwoCommandsWhenTwoUsersArePassed()
    {
        // Given
        $userOne = new User('peter', 'test1@test.pl', '505070800', ['email']);
        $userTwo = new User('boris', 'test2@test.pl', '606070800', ['sms']);
        $this->userRepository
            ->expects($this->once())
            ->method('findByLogin')
            ->willReturn([$userOne, $userTwo]);
        $handlerUnderTest = new CreateNotificationHandler($this->userRepository, $this->bus);
        $command = new CreateNotification([$userOne->getLogin(), $userTwo->getLogin()], 'Hello world');

        // When & Then
        $this->bus
            ->expects($this->exactly(2))
            ->method('dispatch')
            ->with(
                $this->callback(
                    function ($command) use ($userOne, $userTwo) {
                        return
                            ($command instanceof SendEmail && $command->getUserDTO()->getLogin() === $userOne->getLogin())
                            || ($command instanceof SendSms && $command->getUserDTO()->getLogin() === $userTwo->getLogin());
                    }
                ))
            ->willReturnCallback(function ($command) {
                return new Envelope($command);
            });

        $handlerUnderTest($command);
    }
}