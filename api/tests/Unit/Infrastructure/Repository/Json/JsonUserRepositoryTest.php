<?php
/** @author: Roman Kowalski */

namespace App\Tests\Unit\Infrastructure\Repository\Json;

use App\Domain\Entity\User;
use App\Infrastructure\Repository\JsonUserRepository;
use PHPUnit\Framework\TestCase;

class JsonUserRepositoryTest extends TestCase
{
    /**
     * @var JsonUserRepository|\PHPUnit\Framework\MockObject\MockObject|JsonUserRepository
     */
    private $repositoryUnderTest;

    protected function setUp(): void
    {
        $this->repositoryUnderTest = $this->getMockBuilder(JsonUserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getRawData'])
            ->getMock();

        $this->repositoryUnderTest->method('getRawData')->willReturn($this->getUsersData());
    }
    
    public function testShouldFindAllUsers()
    {
        // Given & When
        $actualUsers = $this->repositoryUnderTest->findAll();
        // Then
        $this->assertCount(3, $actualUsers);
    }

    public function testShouldFindUsersByLogin()
    {
        // Given
        $loginCollectionUnderTest = ["ewa", "anna"];

        // When
        /** @var User[] $actual */
        $actual = $this->repositoryUnderTest->findByLogin($loginCollectionUnderTest);

        // Then
        $this->assertCount(2, $actual);
        $this->assertContains($actual[0]->getLogin(), $loginCollectionUnderTest);
        $this->assertContains($actual[1]->getLogin(), $loginCollectionUnderTest);
    }

    public function testShouldReturnEmptyArrayWhenUserIsNotFound()
    {
        // Given
        $loginCollectionUnderTest = ["wojtek"];

        // When
        $actual = $this->repositoryUnderTest->findByLogin($loginCollectionUnderTest);

        // Then
        $this->assertEmpty($actual);
    }

    private function getUsersData()
    {
        return [
            [
                "login" => "anna",
                "email" => "a.nowak@test.pl",
                "phone" => "+48505789789",
                "channels" => ["sms", "email"]
            ],
            [
                "login" => "wiktor",
                "email" => "w.kokosz@test.pl",
                "phone" => "+48601800800",
                "channels" => ["sms"]
            ],
            [
                "login" => "ewa",
                "email" => "e.swoboda@test.pl",
                "phone" => "+48600700700",
                "channels" => ["email", "sms"]
            ]
        ];
    }
}